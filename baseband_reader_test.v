module baseband_reader_test(
    input clkin,
    output sd_clk,
    inout sd_cmd,
    inout [3:0] sd_dat,
    output LED1, LED2,
    output Wing_C0, Wing_C1, Wing_C2, Wing_C3, Wing_C4, Wing_C5, Wing_C6
    );

    wire clk=clkin;
    assign Wing_C0=1'b0;

    /* reset control */
    reg [5:0] reset_count='d0;
    wire reset=&reset_count;
    always @(posedge clk) if(!reset) reset_count <= reset_count + 1'b1;

    reg last_idle=1'b0;
    reg last_idle2=1'b0;
    always @(posedge clk) last_idle <= LED1;
    always @(posedge clk) last_idle2 <= last_idle;

    reg [10:0] counter='d0;
    always @(posedge clk) if(LED1 && !last_idle) counter <= counter + 1'b1;

    /**/assign Wing_C1=(last_idle2 && last_idle && LED1)?1'b1:1'b0;//(counter=={11{1'b1}})?1'b1:1'b0;//1'b0;
    assign Wing_C2=1'b0;
    assign Wing_C3=1'b0;
    assign Wing_C4=1'b0;
    assign Wing_C5=1'b0;/**/
    assign Wing_C6=LED1;

    baseband_reader br(.clk(clk), .reset(reset),
                       .sd_clk(sd_clk), .sd_cmd(sd_cmd), .sd_data(sd_dat),
                       .data_valid(/*Wing_C1*/), .I(/*{Wing_C2, Wing_C3}*/), .Q(/*{Wing_C4, Wing_C5}*/),
                       //.data_valid(Wing_C1), .I({Wing_C2, Wing_C3}), .Q({Wing_C4, Wing_C5}),
                       .start_block(32'd0), .number_of_blocks(32'd0), .enable_playback(1'b1),
                           .allow_restart(1'b0),
                       .idle(LED1), .error(LED2));

endmodule
