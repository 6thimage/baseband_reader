`timescale 1ns / 1ps

module sd_controller(
     input             clk,
     input             reset,
    /* connections to sd card */
    output             sclk,
     inout             cmd,
     inout      [3:0]  data,
     input             card_detect,
    /* read / write ram */
    output             ram_clk,
    output             ram_enable,
    output             ram_write_enable,
    output      [8:0]  ram_address,
    output      [7:0]  ram_data_in,
     input      [7:0]  ram_data_out,
    /* data command control */
     input             data_command_enable,
     input             write_block,
     input             erase_blocks,
     input      [31:0] block_address,
     input             multiple_blocks,
     input             fixed_count,
     input      [31:0] block_count,
     input             last_block,
     input             data_next,
    /* control outputs */
     output reg [3:0]  error,
     output            idle,
     output            data_next_idle,
     output reg        card_version_2,
     output reg        card_hc,
     output reg        card_uhs,
     output reg [31:0] card_block_size
    );
    
    /* initial outputs */
    initial
    begin
        card_version_2=1'b0;
        card_hc=1'b0;
        card_uhs=1'b0;
        card_block_size=32'd0;
        error='d0;
    end

    /* parameters */
    parameter clk_freq=50_000_000;
    parameter sclk_init_freq=400_000;
    parameter sclk_data_freq=25_000_000;
    parameter init_timeout=1;
    parameter data_timeout=0.01;
    parameter SDXC_high_power=1; /* if zero, limits to 0.36 W (100 mA @ 3.6 V),
                                    if 1, allowed to use 0.54 W (150 mA @ 3.6 V) */

    /* local parameters */
    /* clock dividers */
    localparam init_clock_div=(clk_freq/(2*sclk_init_freq))-1;
    localparam data_clock_div=(clk_freq/(2*sclk_data_freq))-1;
    /* response timeouts */
    localparam response_timeout_init=init_timeout*sclk_init_freq;
    localparam response_timeout_data=data_timeout*sclk_data_freq;
    /* divider and timeout lengths */
    localparam clock_div_len=log2(init_clock_div);
    localparam response_timeout_len=log2(response_timeout_data);

    /* errors */
    localparam ERROR_NONE=0, ERROR_TIMEOUT=1, ERROR_RESP=2, ERROR_CRC=3, ERROR_CSD=4, ERROR_LOCKED=5,
               ERROR_DATA=6, ERROR_INVALID_CMD=7, ERROR_ERASE_PARAMS=8, ERROR_UNKNOWN=9;

    /* clock controller */
    wire clock_control_reset;
    reg init_clock=1'b1;
    wire [clock_div_len-1:0] clock_div;
    assign clock_div=(init_clock)?init_clock_div:data_clock_div;
    wire clock_strobe;
    clk_strobe #(.div_len(clock_div_len)) clock_control(.clk(clk), .reset(clock_control_reset),
                                                       .clk_div(clock_div), .clk_strobe(clock_strobe));

    /* dummy clock */
    reg dummy_reset=1'b0;
    wire dummy_enable;
    wire dummy_done;
    wire dummy_sclk_en;
    assign dummy_enable=(clock_strobe && !sclk);
    assign dummy_sclk_en=(dummy_reset && !dummy_done);
    mod_m_counter #(.m(80), .reset_pulse(0)) clk_dummy(.clk(clk), .reset(dummy_reset),
                                                       .enable(dummy_enable), .hit(dummy_done));

    /* sclk control */
    wire sclk_en;
    wire sclk_busy;
    wire sclk_last_clocks_prevent;
    /* uses the reset of the sd_controller module, rather than being independently controlled */
    sclk_controller sclk_control(.clk(clk), .reset(reset), .clock_strobe(clock_strobe),
                                 .enable(sclk_en), .last_clocks_prevent(sclk_last_clocks_prevent),
                                 .sclk(sclk), .last_clocks(sclk_busy));

    /* sd command */
    reg command_reset=1'b0;
    wire command_sclk_en;
    wire command_cmd_out;
    wire command_cmd_enable;
    reg command_enable=1'b0;
    reg [5:0] command_cmd=6'd0;
    reg [31:0] command_arg=32'd0;
    reg [1:0] command_resp;
    wire command_no_resp;
    wire command_busy;
    wire command_crc_error;
    wire command_timeout_error;
    wire [119:0] command_response;
    
    /* assignment of response timeout */
    wire [response_timeout_len-1:0] response_timeout;
    assign response_timeout=(init_clock)?$rtoi(response_timeout_init):$rtoi(response_timeout_data);

    /* command no response assignment
     * as cmd 0 is the only command we don't expect a response from,
     * we can use an assignment instead of having an additional register
     */
    assign command_no_resp=(command_cmd=='d0);

    sd_command #(.response_timeout_len(response_timeout_len))
               command(.clk(clk), .reset(command_reset),
                       .sclk(sclk), .sclk_en(command_sclk_en),
                           .sclk_last_clocks_prevent(sclk_last_clocks_prevent),
                       .cmd_in(cmd), .cmd_out(command_cmd_out), .cmd_enable(command_cmd_enable),
                       .enable(command_enable), .clock_strobe(clock_strobe), .command(command_cmd),
                           .argument(command_arg),
                       .response_type(command_resp), .no_response(command_no_resp),
                           .response_timeout(response_timeout),
                       .busy(command_busy), .error_crc(command_crc_error),
                           .error_timeout(command_timeout_error), .response(command_response)
                      );
    /* response types */
    localparam resp_type_R1=2'b01, resp_type_R2=2'b10, resp_type_R3=2'b00, resp_type_R6=2'b01,
               resp_type_R7=2'b01;

    /* response decoding */
    //wire [5:0]   response_command;          assign response_command=command_response[37:32];
    wire [31:0]  response_R1_status;        assign response_R1_status=command_response[31:0];
    wire [119:0] response_R2_cid_csd;       assign response_R2_cid_csd=command_response[119:0];
    wire [31:0]  response_R3_ocr;           assign response_R3_ocr=command_response[31:0];
    wire [15:0]  response_R6_rca;           assign response_R6_rca=command_response[31:16];
    //wire [15:0]  response_R6_status;        assign response_R6_status=command_response[15:0];
    wire [3:0]   response_R7_voltage;       assign response_R7_voltage=command_response[11:8];
    wire [7:0]   response_R7_check_pattern; assign response_R7_check_pattern=command_response[7:0];
    /* in depth decoding */
    /* R1 */
    wire R1_out_of_range;           assign R1_out_of_range=response_R1_status[31];
    wire R1_address_error;          assign R1_address_error=response_R1_status[30];
    wire R1_erase_param;            assign R1_erase_param=response_R1_status[27];
    wire R1_wp_violation;           assign R1_wp_violation=response_R1_status[26];
    wire R1_status_card_locked;     assign R1_status_card_locked=response_R1_status[25];
    wire R1_card_ecc_failed;        assign R1_card_ecc_failed=response_R1_status[21];
    wire R1_status_card_error;      assign R1_status_card_error=response_R1_status[20];
    wire R1_status_general_error;   assign R1_status_general_error=response_R1_status[19];
    /* OCR */
    wire OCR_initialised;           assign OCR_initialised=response_R3_ocr[31];
    wire OCR_card_capacity;         assign OCR_card_capacity=response_R3_ocr[30];
    wire OCR_UHS_card;              assign OCR_UHS_card=response_R3_ocr[29];
    /* CSD */
    wire [1:0] CSD_version;         assign CSD_version=response_R2_cid_csd[119:118];
    localparam CSD_version_1=2'b00, CSD_version_2=2'b01;
    /*wire [3:0] CSD_1_read_bl_len;   assign CSD_1_read_bl_len=responser_R2_cid_csd[75:72];
    wire [11:0] CSD_1_c_size;       assign CSD_1_c_size=response_R2_cid_csd[65:54];
    wire [2:0] CSD_1_c_size_mult;   assign CSD_1_c_size_mult=response_R2_cid_csd[41:39];*/
    wire [21:0] CSD_2_c_size;       assign CSD_2_c_size=response_R2_cid_csd[61:40];

    localparam card_idle=4'd0, card_ready=4'd1, card_ident=4'd2, card_standby=4'd3, card_tran=4'd4,
               card_data=4'd5, card_receive=4'd6, card_program=4'd7, card_disconnected=4'd8;

    /* sd data */
    reg data_reset=1'b0;
    wire [3:0] data_out;
    wire data_output_enable;
    reg data_enable=1'b0;
    reg data_write_data=1'b0;
    wire data_busy;
    wire data_response_busy;
    wire [2:0] data_error;
    sd_data data_control(.clk(clk), .reset(data_reset), .sclk(sclk), .data_in(data),
                             .data_out(data_out), .data_enable(data_output_enable),
                         .ram_clk(ram_clk), .ram_enable(ram_enable),
                             .ram_write_enable(ram_write_enable), .ram_address(ram_address),
                             .ram_read(ram_data_out), .ram_write(ram_data_in),
                         .enable(data_enable), .write_data(data_write_data),
                             .clock_strobe(clock_strobe),
                         .busy(data_busy), .response_busy(data_response_busy), .error(data_error));
    wire data_sclk_en;
    /* sclk normally waits for data[0] to go high (waiting for the busy part of the R1b response,
     * this causes a problem when interrupting a read (as is necessary with read multiple),
     * hence this bit of a hacked solution - it would be better to change sclk to have a wait
     * for busy signal, but this is a easier and equal
     */
    reg sclk_ignore_data_lines=1'b0;
    assign data_sclk_en=(data_busy||(data_response_busy && !sclk_ignore_data_lines));

    /* */
    reg erase_sclk_enable=1'b0;
    assign clock_control_reset=(dummy_reset||command_busy||data_reset||sclk_busy||erase_sclk_enable);
    assign sclk_en=(dummy_sclk_en||command_sclk_en||data_sclk_en);

    /* cmd mux */
    assign cmd=(dummy_reset)?1'b1:((command_cmd_enable)?command_cmd_out:1'bz);

    /* data mux */
    assign data=(data_output_enable)?data_out:4'bzzzz;

    /* useful constants */
    localparam cmd8_check_pattern=8'b10110100; /* any pattern here will do */
    localparam cmd8_accepted_voltage=4'b0001; /* this bit set means 2.7->3.6 V */
    localparam cmd41_ocr_voltage=9'b000100000;

    /* RCA register */
    reg [15:0] card_rca=16'd0;

    /* state logic */
    localparam STATE_NO_CARD=0, STATE_SD_DETECTED=1, STATE_SD_INIT=2, STATE_IDLE=3,
               STATE_READ_SINGLE=4, STATE_WRITE_SINGLE=5, STATE_READ_MULTIPLE=6,
               STATE_WRITE_MULTIPLE=7, STATE_ERASE=8, STATE_ERROR=9;
    reg [3:0] state=STATE_NO_CARD;

    assign idle=((state==STATE_IDLE)||(state==STATE_ERROR));

    localparam INIT_SEND_IDLE=0, INIT_SEND_IF=1, INIT_PROCESS_IF=2, INIT_SEND_APP_CMD_OP_COND=3,
               INIT_SEND_OP_COND=4, INIT_CARD_READY=5, INIT_SEND_CID=6, INIT_SEND_ADDR=7,
               INIT_SEND_CSD=8, INIT_PROCESS_CSD=9, INIT_SELECT_CARD=10, INIT_UNLOCK_CARD=11,
               INIT_SEND_APP_CMD_BUS_WIDTH=12, INIT_SEND_BUS_WIDTH=13, INIT_COMPLETE=14;
    reg [3:0] init_state=INIT_SEND_IDLE;

    localparam RW_SEND_CMD=0, RW_COUNT=1, RW_PROCESS_RESP=2, RW_DATA=3, RW_DATA_IDLE=4, RW_STOP=5,
               RW_STATUS_CHECK=6;
    localparam ER_SEND_START=0, ER_PROCESS_START=1, ER_SEND_END=2, ER_PROCESS_END=3, ER_ERASE=4;
    reg [2:0] rw_state=RW_SEND_CMD;

    assign data_next_idle=erase_blocks?(rw_state==ER_SEND_END):(rw_state==RW_DATA_IDLE);

    always @(posedge clk or negedge reset)
    begin
        if(!reset)
        begin
            /* reset states */
            state <= STATE_NO_CARD;
            init_state <= INIT_SEND_IDLE;
            rw_state <= RW_SEND_CMD;
            
            /* reset outputs */
            card_version_2 <= 1'b0;
            card_hc <= 1'b0;
            card_uhs <= 1'b0;
            card_block_size <= 32'd0;

            /* reset card rca */
            card_rca <= 16'd0;

            /* reset dummy clock */
            dummy_reset <= 1'b0;

            /* reset init clock */
            init_clock <= 1'b1;

            /* reset command control */
            command_reset <= 1'b0;
            command_enable <= 1'b0;
            command_cmd <= 6'd0;
            command_arg <= 32'd0;

            /* reset data control */
            data_reset <= 1'b0;
            data_enable <= 1'b0;
            data_write_data <= 1'b0;

            /* reset data[0] ignore variable */
            sclk_ignore_data_lines <= 1'b0;

            /* disable erase sclk */
            erase_sclk_enable <= 1'b0;

            /* reset error */
            error <= 'd0;
        end
        else
        begin
            case(state)
            STATE_NO_CARD:
            begin
                if(card_detect)
                begin
                    state <= STATE_SD_DETECTED;

                    /* reset outputs */
                    card_version_2 <= 1'b0;
                    card_hc <= 1'b0;
                    card_uhs <= 1'b0;

                    /* reset card rca */
                    card_rca <= 16'd0;

                    /* reset error */
                    error <= 'd0;

                    /* use init clock */
                    init_clock <= 1'b1;

                    /* start transmission of dummy clocks */
                    dummy_reset <= 1'b1;
                end
            end

            STATE_SD_DETECTED:
            begin
                /* wait for dummy clocks to complete */
                if(dummy_done)
                begin
                    /* disable dummy clocks */
                    dummy_reset <= 1'b0;

                    /* move to next state */
                    state <= STATE_SD_INIT;
                    init_state <= INIT_SEND_IDLE;
                end
            end

            STATE_SD_INIT:
            begin
                if((init_state==INIT_SEND_IDLE)||(init_state==INIT_SEND_APP_CMD_OP_COND)||
                    (init_state==INIT_SEND_CID)||(init_state==INIT_UNLOCK_CARD)||
                    (init_state==INIT_SEND_APP_CMD_BUS_WIDTH))
                begin
                    case(init_state)
                    INIT_SEND_IDLE:
                    begin
                        /* remove command from reset */
                        command_reset <= 1'b1;

                        /* send cmd 0, with no argument, expecting no response
                         * (automatically handled by command) */
                        command_cmd <= 6'd0;
                        command_arg <= 32'd0;
                        command_enable <= 1'b1;

                        /* move to next state */
                        init_state <= INIT_SEND_IF;
                    end

                    INIT_SEND_APP_CMD_OP_COND:
                    begin
                        /* send command 55, expecting R1, with argument */
                        command_cmd <= 6'd55;
                        command_arg <= 32'd0;
                        command_resp <= resp_type_R1;
                        command_enable <= 1'b1;

                        /* move to next state */
                        init_state <= INIT_SEND_OP_COND;
                    end

                    INIT_SEND_CID:
                    begin
                        /* send command 2, expecting R2, with no argument */
                        command_cmd <= 6'd2;
                        command_resp <= resp_type_R2;
                        command_enable <= 1'b1;

                        /* move to next state */
                        init_state <= INIT_SEND_ADDR;
                    end

                    INIT_UNLOCK_CARD:
                    begin
                        /* locked cards currently not supported */
                        error <= ERROR_LOCKED;
                        state <= STATE_ERROR;
                    end

                    INIT_SEND_APP_CMD_BUS_WIDTH:
                    begin
                        /* send app_cmd - command 55, expecting R1, with rca in argument */
                        command_cmd <= 6'd55;
                        command_arg <= {card_rca, 16'h0};
                        command_resp <= resp_type_R1;
                        command_enable <= 1'b1;

                        /* move to next state */
                        init_state <= INIT_SEND_BUS_WIDTH;
                    end

                    default:
                    begin
                        error <= ERROR_UNKNOWN;
                        state <= STATE_ERROR;
                    end
                    endcase
                end
                else
                begin
                    if(command_busy && command_enable)
                        command_enable <= 1'b0;
                    else if(!command_busy && !command_enable && !sclk_busy)
                    begin
                        case(init_state)
                        INIT_SEND_IF:
                        begin
                            /* as no response from cmd 0, there is nothing to check */

                            /* send command 8, expecting R7, with argument */
                            command_cmd <= 6'd8;
                            command_arg <= {20'd0, cmd8_accepted_voltage, cmd8_check_pattern};
                            command_resp <= resp_type_R7;
                            command_enable <= 1'b1;

                            /* move to next state */
                            init_state <= INIT_PROCESS_IF;
                        end

                        INIT_PROCESS_IF:
                        begin
                            /* error checking */
                            if(command_crc_error)
                            begin
                                error <= ERROR_CRC;
                                state <= STATE_ERROR;
                            end
                            else if(command_timeout_error)
                            begin
                                /* version 1 card, or version 2 with bad voltage */
                                card_version_2 <= 1'b0;

                                /* move to next state */
                                init_state <= INIT_SEND_APP_CMD_OP_COND;
                            end
                            else
                            begin
                                /* version 2+ card */
                                if(response_R7_check_pattern!=cmd8_check_pattern ||
                                   response_R7_voltage!=cmd8_accepted_voltage)
                                begin
                                    /* invalid response */
                                    error <= ERROR_RESP;
                                    state <= STATE_ERROR;
                                end
                                else
                                begin
                                    card_version_2 <= 1'b1;

                                    /* move to next state */
                                    init_state <= INIT_SEND_APP_CMD_OP_COND;
                                end
                            end
                        end

                        INIT_SEND_OP_COND:
                        begin
                            /* error checking */
                            if(command_crc_error || command_timeout_error || R1_status_card_error ||
                                R1_status_general_error)
                            begin
                                if(command_crc_error)
                                    error <= ERROR_CRC;
                                else if(command_timeout_error)
                                    error <= ERROR_TIMEOUT;
                                else
                                    error <= ERROR_RESP;
                                state <= STATE_ERROR;
                            end
                            else
                            begin
                                /* send command A41, expecting R3, with argument */
                                command_cmd <= 6'd41;
                                             /* busy,   high-cap support,   fast-boot (eSD only), */
                                command_arg <= {1'b0,   card_version_2,     1'b0,
                                             /* SDXC power control,         reserved, */
                                                SDXC_high_power?1'b1:1'b0,  3'd0,
                                             /* switch to 1.8V / UHS,       OCR voltage bit, */
                                                1'b0,                       cmd41_ocr_voltage,
                                             /* reserved */
                                                15'd0};
                                command_resp <= resp_type_R3;
                                command_enable <= 1'b1;

                                /* move to next state */
                                init_state <= INIT_CARD_READY;
                            end
                        end

                        INIT_CARD_READY:
                        begin
                            /* error checking */
                            if(command_timeout_error)
                            begin
                                error <= ERROR_TIMEOUT;
                                state <= STATE_ERROR;
                            end
                            else if(OCR_initialised) /* check if initialisation is complete */
                            begin
                                /* store card type, if version 2+ card */
                                if(card_version_2)
                                begin
                                    card_hc <= OCR_card_capacity;
                                    card_uhs <= OCR_UHS_card;
                                end

                                /* move to next state */
                                init_state <= INIT_SEND_CID;
                            end
                            else
                            begin
                                /* TODO: need to add max number of retries */
                                /* card hasn't finished initialisation, resend command A41 */
                                init_state <= INIT_SEND_APP_CMD_OP_COND;
                            end
                        end

                        INIT_SEND_ADDR:
                        begin
                            /* error checking */
                            if(command_crc_error || command_timeout_error)
                            begin
                                if(command_crc_error)
                                    error <= ERROR_CRC;
                                else if(command_timeout_error)
                                    error <= ERROR_TIMEOUT;
                                state <= STATE_ERROR;
                            end
                            else
                            begin
                                /* send command 3, expecting R6, with no argument */
                                command_cmd <= 6'd3;
                                command_resp <= resp_type_R6;
                                command_enable <= 1'b1;

                                /* move to next state */
                                init_state <= INIT_SEND_CSD;
                            end
                        end

                        INIT_SEND_CSD:
                        begin
                            /* error checking */
                            if(command_crc_error || command_timeout_error)
                            begin
                                if(command_crc_error)
                                    error <= ERROR_CRC;
                                else if(command_timeout_error)
                                    error <= ERROR_TIMEOUT;
                                state <= STATE_ERROR;
                            end
                            else
                            begin
                                /* copy published rca to register */
                                card_rca <= response_R6_rca;

                                /* send command 9, expecting R2, with rca in argument */
                                command_cmd <= 6'd9;
                                command_arg <= {response_R6_rca, 16'h0}; /* using rca from R6 as
                                                                           card_rca is not valid yet */
                                command_resp <= resp_type_R2;
                                command_enable <= 1'b1;

                                /* move to next state */
                                init_state <= INIT_PROCESS_CSD;
                            end
                        end

                        INIT_PROCESS_CSD:
                        begin
                            /* error checking */
                            if(command_crc_error || command_timeout_error)
                            begin
                                if(command_crc_error)
                                    error <= ERROR_CRC;
                                else if(command_timeout_error)
                                    error <= ERROR_TIMEOUT;
                                state <= STATE_ERROR;
                            end
                            else
                            begin
                                /* process CSD */
                                if(CSD_version==CSD_version_1)
                                begin
                                    /* csd version 1
                                     *     capacity = num_blocks * block_len
                                     *     num_blocks = (c_size + 1) * 2^(c_size_mult + 2)
                                     *     block_len = 2^(read_bl_len)
                                     */

                                    /* reject cards that cannot read / write in 512 KB blocks */
                                    //if(CSD_1_read_bl_len!=4'd9)

                                    /* currently reject all csd version 1 cards
                                     * TODO: support csd version 1 */
                                    error <= ERROR_CSD;
                                    state <= STATE_ERROR;
                                end
                                else if(CSD_version==CSD_version_2)
                                begin
                                    /* csd version 2
                                     *     capacity = (c_size + 1) * 512 KB
                                     *         here KB is 1024 B
                                     *         (implied by SDXC minimum capacity on the top of
                                     *          page 124 in the physical layer simplified
                                     *          specification v4.10)
                                     */

                                    /* copy card size to output */
                                    card_block_size <= (CSD_2_c_size+1)<<10;

                                    /* can now increase the clock rate
                                     * for 50+ MHz, cmd 6 (switch function) needs to be issued
                                     *
                                     * TODO: add logic for cmd 6 if necessary (based on localparam)
                                     */
                                    init_clock <= 1'b0;

                                    /* send select card command - command 7, expecting R1b,
                                     * with rca in argument */
                                    command_cmd <= 6'd7;
                                    command_arg <= {card_rca, 16'h0};
                                    command_resp <= resp_type_R1;
                                    command_enable <= 1'b1;

                                    /* move to next state */
                                    init_state <= INIT_SELECT_CARD;
                                end
                                else
                                begin
                                    /* csd version not supported */
                                    error <= ERROR_CSD;
                                    state <= STATE_ERROR;
                                end
                            end
                        end

                        INIT_SELECT_CARD:
                        begin
                            if(!data_response_busy)
                            begin
                                /* error checking */
                                if(command_crc_error || command_timeout_error || R1_status_card_error
                                    || R1_status_general_error)
                                begin
                                    if(command_crc_error)
                                    error <= ERROR_CRC;
                                    else if(command_timeout_error)
                                        error <= ERROR_TIMEOUT;
                                    else
                                        error <= ERROR_RESP;
                                    state <= STATE_ERROR;
                                end
                                else
                                begin
                                    /* check if card is locked */
                                    if(R1_status_card_locked)
                                        /* move to unlock state */
                                        init_state <= INIT_UNLOCK_CARD;
                                    else
                                        /* move to next state */
                                        init_state <= INIT_SEND_APP_CMD_BUS_WIDTH;
                                end
                            end
                        end

                        INIT_SEND_BUS_WIDTH:
                        begin
                            /* error checking */
                            if(command_crc_error || command_timeout_error || R1_status_card_error ||
                                R1_status_general_error)
                            begin
                                if(command_crc_error)
                                    error <= ERROR_CRC;
                                else if(command_timeout_error)
                                    error <= ERROR_TIMEOUT;
                                else
                                    error <= ERROR_RESP;
                                state <= STATE_ERROR;
                            end
                            else
                            begin
                                /* send app command 6, expecting R1, with bus width in argument */
                                command_cmd <= 6'd6;
                                command_arg <= {30'd0, 2'b10};
                                command_resp <= resp_type_R1;
                                command_enable <= 1'b1;

                                /* move to next state */
                                init_state <= INIT_COMPLETE;
                            end
                        end

                        INIT_COMPLETE:
                        begin
                            /* error checking */
                            if(command_crc_error || command_timeout_error)
                            begin
                                if(command_crc_error)
                                    error <= ERROR_CRC;
                                else if(command_timeout_error)
                                    error <= ERROR_TIMEOUT;
                                state <= STATE_ERROR;
                            end
                            else
                            begin
                                /* TODO: verify state of sd card */

                                /* move to idle state */
                                state <= STATE_IDLE;
                            end
                        end

                        default:
                        begin
                            error <= ERROR_UNKNOWN;
                            state <= STATE_ERROR;
                        end
                        endcase
                    end
                end
            end

            STATE_IDLE:
            begin
                if(data_command_enable)
                begin
                    case({erase_blocks, multiple_blocks, write_block})
                    3'b000: state <= STATE_READ_SINGLE;
                    3'b001: state <= STATE_WRITE_SINGLE;
                    3'b010: state <= STATE_READ_MULTIPLE;
                    3'b011: state <= STATE_WRITE_MULTIPLE;
                    3'b100: state <= STATE_ERASE;
                    default:
                    begin
                        state <= STATE_ERROR;
                        error <= ERROR_INVALID_CMD;
                    end
                    endcase

                    rw_state <= RW_SEND_CMD;
                end
            end

            STATE_READ_SINGLE:
            begin
                /* process
                 *
                 * - send read command - cmd 17, arg [31:0] address
                 * - check command response - R1
                 * - receive data
                 * - check crc matches data
                 */
                case(rw_state)
                RW_SEND_CMD:
                begin
                    /* send command 17, expecting R1, with block address in argument */
                    command_cmd <= 6'd17;
                    command_arg <= block_address;
                    command_resp <= resp_type_R1;
                    command_enable <= 1'b1;

                    /* move to next state */
                    rw_state <= RW_PROCESS_RESP;
                end

                RW_PROCESS_RESP:
                begin
                    if(command_busy && command_enable)
                        command_enable <= 1'b0;
                    else if(!command_busy && !command_enable && !sclk_busy)
                    begin
                        /* error checking */
                        if(command_crc_error || command_timeout_error)
                        begin
                            if(command_crc_error)
                                error <= ERROR_CRC;
                            else if(command_timeout_error)
                                error <= ERROR_TIMEOUT;

                            /* move to error state */
                            state <= STATE_ERROR;
                        end
                        else
                        begin
                            /* start receiving data */
                            data_reset <= 1'b1;
                            data_enable <= 1'b1;
                            data_write_data <= 1'b0;

                            /* move to next state */
                            rw_state <= RW_DATA;
                        end
                    end
                end

                RW_DATA:
                begin
                    if(data_busy && data_enable)
                        data_enable <= 1'b0;
                    else if(!data_busy && !data_enable && !data_response_busy && !sclk_busy)
                    begin

                        /* disable data control */
                        data_reset <= 1'b0;

                        /* check for write error */
                        if(data_error!='d0)
                        begin
                            error <= ERROR_DATA;
                            state <= STATE_ERROR;
                        end
                        else
                        begin
                            /* move to idle state */
                            state <= STATE_IDLE;
                        end
                    end
                end
                endcase
            end

            STATE_WRITE_SINGLE:
            begin
                /* process
                 *
                 * - send write command - cmd 24, arg [31:0] address
                 * - check command response - R1
                 * - send data
                 * - check data response
                 * - check status of card (i.e. confirm write)
                 */
                case(rw_state)
                RW_SEND_CMD:
                begin
                    /* send command 24, expecting R1, with block address in argument */
                    command_cmd <= 6'd24;
                    command_arg <= block_address;
                    command_resp <= resp_type_R1;
                    command_enable <= 1'b1;

                    /* move to next state */
                    rw_state <= RW_PROCESS_RESP;
                end

                RW_PROCESS_RESP:
                begin
                    if(command_busy && command_enable)
                        command_enable <= 1'b0;
                    else if(!command_busy && !command_enable && !sclk_busy)
                    begin
                        /* error checking */
                        if(command_crc_error || command_timeout_error)
                        begin
                            if(command_crc_error)
                                error <= ERROR_CRC;
                            else if(command_timeout_error)
                                error <= ERROR_TIMEOUT;
                            state <= STATE_ERROR;
                        end
                        else
                        begin
                            /* start data sending */
                            data_reset <= 1'b1;
                            data_enable <= 1'b1;
                            data_write_data <= 1'b1;

                            /* move to next state */
                            rw_state <= RW_DATA;
                        end
                    end
                end

                RW_DATA:
                begin
                    if(data_busy && data_enable)
                        data_enable <= 1'b0;
                    else if(!data_busy && !data_enable && !data_response_busy && !sclk_busy)
                    begin
                        /* disable data control */
                        data_reset <= 1'b0;

                        /* check for write error */
                        if(data_error!='d0)
                        begin
                            error <= ERROR_DATA;
                            state <= STATE_ERROR;
                        end
                        else
                        begin
                            /* send status check command - command 13, expecting R1,
                             * with rca in argument */
                            command_cmd <= 6'd13;
                            command_arg <= {card_rca, 16'h0};
                            command_resp <= resp_type_R1;
                            command_enable <= 1'b1;

                            /* move to next state */
                            rw_state <= RW_STATUS_CHECK;
                        end
                    end
                end

                RW_STATUS_CHECK:
                begin
                    if(command_busy && command_enable)
                        command_enable <= 1'b0;
                    else if(!command_busy && !command_enable && !sclk_busy && !data_busy)
                    begin
                        /* error checking */
                        if(command_crc_error || command_timeout_error || R1_out_of_range ||
                            R1_address_error || R1_wp_violation || R1_card_ecc_failed ||
                            R1_status_card_error || R1_status_general_error)
                        begin
                            if(command_crc_error)
                                error <= ERROR_CRC;
                            else if(command_timeout_error)
                                error <= ERROR_TIMEOUT;
                            else if(R1_out_of_range || R1_address_error || R1_wp_violation ||
                                    R1_card_ecc_failed || R1_status_card_error ||
                                    R1_status_general_error)
                                error <= ERROR_DATA;
                            state <= STATE_ERROR;
                        end
                        else
                        begin
                            /* move to idle state */
                            state <= STATE_IDLE;
                        end
                    end
                end
                endcase
            end

            STATE_READ_MULTIPLE:
            begin
                /* process
                 *
                 * - send read command - cmd 18, arg [31:0] address
                 * - check command respnse
                 * - repeatedly
                 *     - read data
                 * - send stop command
                 */
                case(rw_state)
                RW_SEND_CMD:
                begin
                    if(fixed_count)
                    begin
                        /* command 23, expecting R1, with count in argument */
                        command_cmd <= 6'd23;
                        command_arg <= block_count;
                        command_resp <= resp_type_R1;
                        command_enable <= 1'b1;

                        /* move to next state */
                        rw_state <= RW_COUNT;
                    end
                    else
                    begin
                        /* send command 18, expecting R1, with block address in argument */
                        command_cmd <= 6'd18;
                        command_arg <= block_address;
                        command_resp <= resp_type_R1;
                        command_enable <= 1'b1;

                        /* move to next state */
                        rw_state <= RW_PROCESS_RESP;
                    end
                end

                RW_COUNT:
                begin
                    if(command_busy && command_enable)
                        command_enable <= 1'b0;
                    else if(!command_busy && !command_enable && !sclk_busy)
                    begin
                        /* error checking */
                        if(command_crc_error || command_timeout_error)
                        begin
                            if(command_crc_error)
                                error <= ERROR_CRC;
                            else if(command_timeout_error)
                                error <= ERROR_TIMEOUT;
                            state <= STATE_ERROR;
                        end
                        else
                        begin
                            /* send command 18, expecting R1, with block address in argument */
                            command_cmd <= 6'd18;
                            command_arg <= block_address;
                            command_resp <= resp_type_R1;
                            command_enable <= 1'b1;

                            /* move to next state */
                            rw_state <= RW_PROCESS_RESP;
                        end
                    end
                end

                RW_PROCESS_RESP:
                begin
                    if(command_busy && command_enable)
                        command_enable <= 1'b0;
                    else if(!command_busy && !command_enable && !sclk_busy)
                    begin
                        /* error checking */
                        if(command_crc_error || command_timeout_error)
                        begin
                            if(command_crc_error)
                                error <= ERROR_CRC;
                            else if(command_timeout_error)
                                error <= ERROR_TIMEOUT;
                            state <= STATE_ERROR;
                        end
                        else
                        begin
                            /* start data sending */
                            data_reset <= 1'b1;
                            data_enable <= 1'b1;
                            data_write_data <= 1'b0;

                            /* move to next state */
                            rw_state <= RW_DATA;
                        end
                    end
                end

                RW_DATA:
                begin
                    if(data_busy && data_enable)
                        /* disable data enable */
                        data_enable <= 1'b0;
                    else if(!data_busy && !data_enable && !data_response_busy && !sclk_busy)
                    begin
                        /* disable data control */
                        data_reset <= 1'b0;

                        /* check for write error */
                        if(data_error!='d0)
                        begin
                            error <= ERROR_DATA;
                            state <= STATE_ERROR;
                        end
                        else
                        begin
                            if(last_block)
                            begin
                                if(!fixed_count)
                                begin
                                    /* send stop - command 12, expecting R1b, with no argument */
                                    command_cmd <= 6'd12;
                                    command_arg <= 32'h0;
                                    command_resp <= resp_type_R1;
                                    command_enable <= 1'b1;
                                    sclk_ignore_data_lines <= 1'b1;
                                end

                                /* move to next state */
                                rw_state <= RW_STOP;
                            end
                            else
                                /* move to next state */
                                rw_state <= RW_DATA_IDLE;
                        end
                    end
                end

                RW_DATA_IDLE:
                begin
                    if(data_next)
                    begin
                        /* start data sending */
                        data_reset <= 1'b1;
                        data_enable <= 1'b1;
                        data_write_data <= 1'b0;

                        rw_state <= RW_DATA;
                    end
                end

                RW_STOP:
                begin
                    if(command_busy && command_enable)
                        command_enable <= 1'b0;
                    else if(!command_busy && !command_enable && !sclk_busy)
                    begin
                        sclk_ignore_data_lines <= 1'b0;
                        /* error checking */
                        if(command_crc_error || command_timeout_error)
                        begin
                            if(command_crc_error)
                                error <= ERROR_CRC;
                            else if(command_timeout_error)
                                error <= ERROR_TIMEOUT;
                            state <= STATE_ERROR;
                        end
                        else
                        begin
                            /* move to next state */
                            state <= STATE_IDLE;
                        end
                    end
                end
                endcase
            end

            STATE_WRITE_MULTIPLE:
            begin
                /* process
                 *
                 * - send write command - cmd 25, arg [31:0] address
                 * - check command response - R1
                 * - repeatedly
                 *     - send data
                 *     - check data response
                 * - send stop command
                 * - check status of card (i.e. confirm write)
                 */
                case(rw_state)
                RW_SEND_CMD:
                begin
                    if(fixed_count)
                    begin
                        /* command 23, expecting R1, with count in argument */
                        command_cmd <= 6'd23;
                        command_arg <= block_count;
                        command_resp <= resp_type_R1;
                        command_enable <= 1'b1;

                        /* move to next state */
                        rw_state <= RW_COUNT;
                    end
                    else
                    begin
                        /* send command 25, expecting R1, with block address in argument */
                        command_cmd <= 6'd25;
                        command_arg <= block_address;
                        command_resp <= resp_type_R1;
                        command_enable <= 1'b1;

                        /* move to next state */
                        rw_state <= RW_PROCESS_RESP;
                    end
                end

                RW_COUNT:
                begin
                    if(command_busy && command_enable)
                        command_enable <= 1'b0;
                    else if(!command_busy && !command_enable && !sclk_busy)
                    begin
                        /* error checking */
                        if(command_crc_error || command_timeout_error)
                        begin
                            if(command_crc_error)
                                error <= ERROR_CRC;
                            else if(command_timeout_error)
                                error <= ERROR_TIMEOUT;
                            state <= STATE_ERROR;
                        end
                        else
                        begin
                            /* send command 25, expecting R1, with block address in argument */
                            command_cmd <= 6'd25;
                            command_arg <= block_address;
                            command_resp <= resp_type_R1;
                            command_enable <= 1'b1;

                            /* move to next state */
                            rw_state <= RW_PROCESS_RESP;
                        end
                    end
                end

                RW_PROCESS_RESP:
                begin
                    if(command_busy && command_enable)
                        command_enable <= 1'b0;
                    else if(!command_busy && !command_enable && !sclk_busy)
                    begin
                        /* error checking */
                        if(command_crc_error || command_timeout_error)
                        begin
                            if(command_crc_error)
                                error <= ERROR_CRC;
                            else if(command_timeout_error)
                                error <= ERROR_TIMEOUT;
                            state <= STATE_ERROR;
                        end
                        else
                        begin
                            /* start data sending */
                            data_reset <= 1'b1;
                            data_enable <= 1'b1;
                            data_write_data <= 1'b1;

                            /* move to next state */
                            rw_state <= RW_DATA;
                        end
                    end
                end

                RW_DATA:
                begin
                    if(data_busy && data_enable)
                        /* disable data enable */
                        data_enable <= 1'b0;
                    else if(!data_busy && !data_enable && !data_response_busy && !sclk_busy)
                    begin
                        /* disable data control */
                        data_reset <= 1'b0;

                        /* check for write error */
                        if(data_error!='d0)
                        begin
                            error <= ERROR_DATA;
                            state <= STATE_ERROR;
                        end
                        else
                        begin
                            if(last_block)
                            begin
                                if(!fixed_count)
                                begin
                                    /* send stop - command 12, expecting R1b, with no argument */
                                    command_cmd <= 6'd12;
                                    command_arg <= 32'h0;
                                    command_resp <= resp_type_R1;
                                    command_enable <= 1'b1;
                                end

                                /* move to next state */
                                rw_state <= RW_STOP;
                            end
                            else
                                /* move to next state */
                                rw_state <= RW_DATA_IDLE;
                        end
                    end
                end

                RW_DATA_IDLE:
                begin
                    if(data_next)
                    begin
                        /* start data sending */
                        data_reset <= 1'b1;
                        data_enable <= 1'b1;
                        data_write_data <= 1'b1;

                        rw_state <= RW_DATA;
                    end
                end

                RW_STOP:
                begin
                    if(command_busy && command_enable)
                        command_enable <= 1'b0;
                    else if(!command_busy && !command_enable && !sclk_busy)
                    begin
                        /* error checking */
                        if(command_crc_error || command_timeout_error)
                        begin
                            if(command_crc_error)
                                error <= ERROR_CRC;
                            else if(command_timeout_error)
                                error <= ERROR_TIMEOUT;
                            state <= STATE_ERROR;
                        end
                        else
                        begin
//                            /* send status check command - command 13, expecting R1,
//                             * with rca in argument */
//                            command_cmd <= 6'd13;
//                            command_arg <= {card_rca, 16'h0};
//                            command_resp <= resp_type_R1;
//                            command_enable <= 1'b1;
//
//                            /* move to next state */
//                            rw_state <= RW_STATUS_CHECK;
                            state <= STATE_IDLE;
                        end
                    end
                end

                RW_STATUS_CHECK:
                begin
                    if(command_busy && command_enable)
                        command_enable <= 1'b0;
                    else if(!command_busy && !command_enable && !sclk_busy)
                    begin
                        /* error checking */
                        if(command_crc_error || command_timeout_error)
                        begin
                            if(command_crc_error)
                                error <= ERROR_CRC;
                            else if(command_timeout_error)
                                error <= ERROR_TIMEOUT;
                            state <= STATE_ERROR;
                        end
                        else
                        begin
                            /* TODO: check status */
                            /* move to idle state */
                            state <= STATE_IDLE;
                        end
                    end
                end
                endcase
            end

            STATE_ERASE:
            begin
                /* process
                 *
                 * - send erase start block (cmd 32)
                 * - send erase end block (cmd 33)
                 * - send erase command (cmd 38)
                 * - check status
                 *     - card will be in the transfer state prior to cmd 38
                 *     - after cmd 38 is issued, the card will enter the programming state
                 *         - whilst the card is being programmed, dat0 will be held low by
                 *           the card (sd spec 4.3.5, pg 38)
                 *     - the card will then, after some time, return to the transfer state
                 */
                case(rw_state)
                    ER_SEND_START:
                    begin
                        /* send command 32, expecting R1, with start address in argument */
                        command_cmd <= 6'd32;
                        command_arg <= block_address;
                        command_resp <= resp_type_R1;
                        command_enable <= 1'b1;

                        /* move to next state */
                        rw_state <= ER_PROCESS_START;
                    end

                    ER_PROCESS_START:
                    begin
                        if(command_busy && command_enable)
                            command_enable <= 1'b0;
                        else if(!command_busy && !command_enable && !sclk_busy)
                        begin
                            /* error checking */
                            if(command_crc_error || command_timeout_error)
                            begin
                                if(command_crc_error)
                                    error <= ERROR_CRC;
                                else if(command_timeout_error)
                                    error <= ERROR_TIMEOUT;
                                state <= STATE_ERROR;
                            end
                            else
                                /* move to next state */
                                rw_state <= ER_SEND_END;
                        end
                    end

                    ER_SEND_END:
                    begin
                        /* wait for data_next flag */
                        if(data_next)
                        begin
                            /* send command 33, expecting R1, with end address in argument */
                            command_cmd <= 6'd33;
                            command_arg <= block_address;
                            command_resp <= resp_type_R1;
                            command_enable <= 1'b1;

                            /* move to next state */
                            rw_state <= ER_PROCESS_END;
                        end
                    end

                    ER_PROCESS_END:
                    begin
                        if(command_busy && command_enable)
                            command_enable <= 1'b0;
                        else if(!command_busy && !command_enable && !sclk_busy)
                        begin
                            /* error checking */
                            if(command_crc_error || command_timeout_error)
                            begin
                                if(command_crc_error)
                                    error <= ERROR_CRC;
                                else if(command_timeout_error)
                                    error <= ERROR_TIMEOUT;
                                state <= STATE_ERROR;
                            end
                            else
                            begin
                                /* send erase command (cmd 38, expecting R1b, no argument */
                                command_cmd <= 6'd38;
                                command_arg <= 32'd0;
                                command_resp <= resp_type_R1;
                                command_enable <= 1'b1;

                                /* move to next state */
                                rw_state <= ER_ERASE;
                            end
                        end
                    end

                    ER_ERASE:
                    begin
                        if(command_busy && command_enable)
                            command_enable <= 1'b0;
                        else if(!command_busy && !command_enable && !sclk_busy)
                        begin
                            /* error checking */
                            if(command_crc_error || command_timeout_error || R1_erase_param)
                            begin
                                if(command_crc_error)
                                    error <= ERROR_CRC;
                                else if(command_timeout_error)
                                    error <= ERROR_TIMEOUT;
                                else if(R1_erase_param)
                                    error <= ERROR_ERASE_PARAMS;
                                state <= STATE_ERROR;
                            end
                            else if(!data_response_busy)
                            begin
                                /* disable sclk */
                                erase_sclk_enable <= 1'b0;

                                /* TODO check if this is okay, or if a status check is necessary */
                                state <= STATE_IDLE;
                            end
                            else
                                /* enable sclk so that data lines are updated */
                                erase_sclk_enable <= 1'b1;
                        end
                    end
                endcase
            end

            STATE_ERROR:
            begin
            end

            default:
            begin
                error <= ERROR_UNKNOWN;
                state <= STATE_ERROR;
            end
            endcase
        end
    end

    /* this function is only used at synthesis to calculate
       the number of bits required to store n */
    function integer log2(input integer n);
        integer i;
        begin
            log2=1;
            for(i=0; 2**i<n; i=i+1)
                log2=i+1;
        end
    endfunction

endmodule
