`timescale 1ns / 1ps

module resp_recv #(
    parameter timeout_size=20
    )
    (
     input                          clk,
     input                          reset,
    /* control */
     input                          enable,
     input     [1:0]                response_type,  /* high bit enables long response, low bit enables crc calculation,
                                                       high bit not internally stored, must be held constant while busy is high */
     input                          no_response,    /* not internally stored, must be held constant while busy is high */
     input     [timeout_size-1:0]   timeout_len,    /* not internally stored, must be held constant while busy is high */
    /* connection to clock divider */
     input                          clock_strobe,
    /* connections to sd card */
     input                          sclk,
    output                          sclk_en,
     input                          cmd_line,
    /* outputs */
    output reg                      busy,
    output reg                      timeout_error,  /* this output is not reset when reset is low */
    output reg                      crc_error,      /* this output is not reset when reset is low */
    output     [119:0]              response        /* this output is not reset when reset is low */
    );

    /* initial outputs */
    initial
    begin
        busy=1'b0;
        timeout_error=1'b0;
        crc_error=1'b0;
    end

    /* internal response register */
    reg [127:0] response_int;
    assign response=response_int[127:8];

    /* crc unit */
    reg crc_reset=1'b0;
    reg crc_enable=1'b0;
    wire [6:0] crc;
    crc7 resp_crc(.clk(clk), .reset(crc_reset), .enable(crc_enable), .data(response_int[7:0]), .crc(crc));

    /* wire for the expected response size */
    wire resp_long;
    assign resp_long=response_type[1];

    /* counters */
    reg [timeout_size-1:0] timeout_counter;
    reg [7:0] bit_counter;

    /* states */
    localparam STATE_IDLE=0, STATE_WAIT=1, STATE_RECV=2, STATE_CRC=3;
    reg [2:0] state=STATE_IDLE;

    assign sclk_en=((state==STATE_WAIT)||(state==STATE_RECV))?1'b1:1'b0;

    /* state machine */
    always @(posedge clk or negedge reset)
    begin
        if(!reset)
        begin
            /* reset outputs */
            busy <= 1'b0;

            /* timeout_error and crc_error are made purposely to not loose their data on
             * reset, both of these are outputs from this module and so this does not affect
             * the functionality of this module. It does, however, have the advantage of
             * reducing the number of registers in the parent of this module
             */

            /* reset crc */
            crc_reset <= 1'b0;

            /* reset internal state */
            state <= STATE_IDLE;
        end
        else
        begin
            case(state)
            STATE_IDLE:
            begin
                if(enable)
                begin
                    if(!no_response)
                    begin
                        /* remove crc from reset if required */
                        crc_reset <= response_type[0];

                        /* move to next state */
                        state <= STATE_WAIT;
                    end
                    else
                    begin
                        /* move to next state */
                        state <= STATE_IDLE;
                    end

                    /* set busy flag */
                    busy <= 1'b1;

                    /* reset error flags */
                    timeout_error <= 1'b0;
                    crc_error <= 1'b0;

                    /* reset timeout counter */
                    timeout_counter <= 'h0;
                end
                else
                    /* ensure that the busy flag is low */
                    busy <= 1'b0;
            end

            STATE_WAIT:
            begin
                if(clock_strobe)
                begin
                    /* data is read on rising edge */
                    if(!sclk)
                    begin
                        /* look for start condition */
                        if(!cmd_line)
                        begin
                            /* reset bit counter */
                            bit_counter <= 8'd0;

                            /* write first bit into response */
                            response_int <= {response_int[126:0], cmd_line};

                            /* move to next state */
                            state <= STATE_RECV;
                        end
                        else
                        begin
                            if(timeout_counter==timeout_len)
                            begin
                                /* command timed out */
                                timeout_error <= 1'b1;

                                /* move to the idle state */
                                state <= STATE_IDLE;
                            end

                            /* increment timeout counter */
                            timeout_counter <= timeout_counter + 1'b1;
                        end
                    end
                end
            end

            STATE_RECV:
            begin
                /* ensure that crc does not stay enabled for more than one clock cycle */
                if(crc_enable)
                    crc_enable <= 1'b0;

                if(clock_strobe)
                begin
                    /* read on rising edge */
                    if(!sclk)
                    begin
                        /* shift bit in */
                        response_int <= {response_int[126:0], cmd_line};

                        /* check for the end condition */
                        if((resp_long && (bit_counter==134))||
                           (!resp_long && (bit_counter==46)))
                        begin
                            /* move to the next state */
                            if(crc_reset)
                                state <= STATE_CRC;
                            else
                                state <= STATE_IDLE;
                        end
                        else if(crc_reset && ((bit_counter%8)==8'd6))
                            /* if end of byte (but not last byte)
                             * enable crc */
                            crc_enable <= 1'b1;

                        /* increment bit counter */
                        bit_counter <= bit_counter + 1'b1;
                    end
                end
            end

            STATE_CRC:
            begin
                /* check crc */
                if(response_int[7:1]!=crc)
                    crc_error <= 1'b1;
                else
                    crc_error <= 1'b0;

                /* place crc module into reset */
                crc_reset <= 1'b0;

                /* move to next state */
                state <= STATE_IDLE;
            end

            default: state <= STATE_IDLE;
            endcase
        end
    end

endmodule
