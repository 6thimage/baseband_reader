`timescale 1ns / 1ps

module sd_data(
     input           clk,
     input           reset,
    /* connections to sd card */
     input           sclk,
     input     [3:0] data_in,
    output reg [3:0] data_out,
    output reg       data_enable,
    /* connections to data ram */
    output           ram_clk,
    output reg       ram_enable,
    output reg       ram_write_enable,
    output reg [8:0] ram_address,
     input     [7:0] ram_read,
    output reg [7:0] ram_write,
    /* connections to control logic */
     input           enable,
     input           write_data,
     input           clock_strobe,
    /* outputs */
    output reg       busy,
    output           response_busy,
    output reg [2:0] error
    );

    localparam ERROR_NONE=3'd0, ERROR_CRC=3'd1, ERROR_NO_STOP_BIT=3'd2, ERROR_NO_END_BIT=3'd2, ERROR_OTHER=3'd4;

    initial
    begin
        data_out=4'hf;
        data_enable=1'b0;
        busy=1'b0;
        error=ERROR_NONE;

        ram_enable=1'b0;
        ram_write_enable=1'b0;
        ram_address=9'h0;
    end

    /* response_busy output - asserted when data_in[0] is held low by card, when not sending data */
    assign response_busy=(!busy && !data_in[0])?1'b1:1'b0;

    /* ram clk set to clk */
    assign ram_clk=clk;

    /* crc generators */
    reg crc_reset=1'b0;
    reg crc_enable=1'b0;
    wire [15:0] crc[3:0];
    /* mux for crc data */
    wire [7:0] crc_data;
    assign crc_data=(ram_write_enable==1)?ram_write:ram_read;

    genvar i;
    generate
        for(i=0; i<4; i=i+1)
        begin : data
            crc16_2 crc_gen(.clk(clk), .reset(crc_reset), .enable(crc_enable), .data({crc_data[i+4], crc_data[i]}), .crc(crc[i]));
        end
    endgenerate

    /* bit counter for crc and status*/
    reg [3:0] bit_counter=4'd15;

    /* write status register */
    reg [2:0] status;

    /* state machine */
    localparam STATE_IDLE=0, STATE_READ=1, STATE_WRITE=2, STATE_STATUS_RECV=3;
    reg [1:0] state=STATE_IDLE;

    localparam RW_START_BIT=0, RW_HIGH_NIBBLE=1, RW_LOW_NIBBLE=2, RW_CRC=3, RW_STOP_BIT=4;
    localparam SR_START=0, SR_START_BIT=1, SR_STATUS=2, SR_END_BIT=3, SR_PROCESS=4;
    reg [2:0] rw_state=RW_START_BIT;

    integer j;

    always @(posedge clk or negedge reset)
    begin
        if(!reset)
        begin
            /* reset outputs */
            data_out <= 4'b1111;
            data_enable <= 1'b0;
            busy <= 1'b0;
            error <= ERROR_NONE;

            /* ram reset */
            ram_enable <= 1'b0;
            ram_write_enable <= 1'b0;
            ram_address <= 9'h0;

            /* crc reset */
            crc_reset <= 1'b0;
            crc_enable <= 1'b0;

            /* reset bit counter */
            bit_counter <= 4'd15;

            /* reset states */
            state <= STATE_IDLE;
            rw_state <= RW_START_BIT;
        end
        else
        begin
            case(state)
            STATE_IDLE:
            begin
                /* if enable */
                if(enable)
                begin
                    /* select state */
                    if(write_data)
                        state <= STATE_WRITE;
                    else
                        state <= STATE_READ;

                    /* reset rw_state */
                    rw_state <= RW_START_BIT;
                    
                    /* reset error */
                    error <= ERROR_NONE;

                    /* set busy flag */
                    busy <= 1'b1;
                end
                else
                    /* otherwise, ensure busy flag is low */
                    busy <= 1'b0;

                /* ensure data_enable is always returned to zero */
                data_enable <= 1'b0;
            end

            STATE_READ:
            begin
                /* read on rising edge */
                if(clock_strobe && !sclk)
                begin
                    case(rw_state)
                    RW_START_BIT:
                    begin
                        /* look for start bit */
                        if(data_in==4'b0000)
                        begin
                            /* set up ram */
                            ram_enable <= 1'b0;
                            ram_write_enable <= 1'b1;
                            ram_address <= 9'd511;

                            /* remove crc from reset */
                            crc_reset <= 1'b1;
                            crc_enable <= 1'b0;

                            /* move to next state */
                            rw_state <= RW_HIGH_NIBBLE;
                        end
                    end

                    RW_HIGH_NIBBLE:
                    begin
                        /* read data */
                        ram_write[7:4] <= data_in;

                        /* ensure ram and crc are disabled */
                        ram_enable <= 1'b0;
                        crc_enable <= 1'b0;

                        /* increment address */
                        ram_address <= ram_address + 1'b1;

                        /* move to next state */
                        rw_state <= RW_LOW_NIBBLE;
                    end

                    RW_LOW_NIBBLE:
                    begin
                        /* read data in */
                        ram_write[3:0] <= data_in;

                        /* enable ram write */
                        ram_enable <= 1'b1;

                        /* enable crc */
                        crc_enable <= 1'b1;

                        /* check for end condition */
                        if(ram_address==9'd511)
                        begin
                            /* reset bit counter */
                            bit_counter <= 4'd15;

                            /* move to next state */
                            rw_state <= RW_CRC;
                        end
                        else
                        begin
                            /* move to high nibble state */
                            rw_state <= RW_HIGH_NIBBLE;
                        end
                    end

                    RW_CRC:
                    begin
                        /* ensure ram and crc are disabled */
                        ram_enable <= 1'b0;
                        crc_enable <= 1'b0;

                        /* check crc */
                        for(j=0; j<4; j=j+1)
                            if(data_in[j]!=crc[j][bit_counter])
                                error <= error | ERROR_CRC;

                        /* increment counter */
                        bit_counter <= bit_counter - 1'b1;

                        /* check for end condition */
                        if(bit_counter==4'd0)
                            /* move to next state */
                            rw_state <= RW_STOP_BIT;
                    end

                    RW_STOP_BIT:
                    begin
                        /* place crc into reset */
                        crc_reset <= 1'b0;

                        /* check stop bit */
                        if(data_in!=4'b1111)
                            error <= error | ERROR_NO_STOP_BIT;

                        /* move to idle state */
                        state <= STATE_IDLE;
                    end
                    endcase
                end
                else
                begin
                    /* disable the ram_enable if it is set */
                    if(ram_enable)
                        ram_enable <= 1'b0;

                    /* disable crc_enable if set */
                    if(crc_enable)
                        crc_enable <= 1'b0;
                end
            end

            STATE_WRITE:
            begin
                /* write on falling edge */
                if(clock_strobe && sclk)
                begin
                    case(rw_state)
                    RW_START_BIT:
                    begin
                        /* send start bit */
                        data_enable <= 1'b1;
                        data_out <= 4'b0000;

                        /* set up ram */
                        ram_enable <= 1'b1;
                        ram_write_enable <= 1'b0;
                        ram_address <= 9'h0;

                        /* remove crc from reset */
                        crc_reset <= 1'b1;

                        /* move to next state */
                        rw_state <= RW_HIGH_NIBBLE;
                    end

                    RW_HIGH_NIBBLE:
                    begin
                        /* write data out */
                        data_out <= ram_read[7:4];

                        /* ensure ram is disabled */
                        ram_enable <= 1'b0;

                        /* enable crc */
                        crc_enable <= 1'b1;

                        /* move to next state */
                        rw_state <= RW_LOW_NIBBLE;
                    end

                    RW_LOW_NIBBLE:
                    begin
                        /* write data out */
                        data_out <= ram_read[3:0];

                        /* check for end condition */
                        if(ram_address==9'd511)
                        begin
                            /* reset crc counter */
                            bit_counter <= 4'd15;
                            
                            /* ensure ram and crc are both disabled */
                            ram_enable <= 1'b0;
                            crc_enable <= 1'b0;

                            /* move to first CRC state */
                            rw_state <= RW_CRC;
                        end
                        else
                        begin
                            /* increment address */
                            ram_address <= ram_address + 1'b1;

                            /* enable ram */
                            ram_enable <= 1'b1;

                            /* ensure crc is disabled */
                            crc_enable <= 1'b0;

                            /* move to high nibble state */
                            rw_state <= RW_HIGH_NIBBLE;
                        end
                    end

                    RW_CRC:
                    begin
                        /* send data */
                        for(j=0; j<4; j=j+1)
                            data_out[j] <= crc[j][bit_counter];

                        /* increment counter */
                        bit_counter <= bit_counter - 1'b1;

                        /* check for end condition */
                        if(bit_counter==4'd0)
                            /* move to next state */
                            rw_state <= RW_STOP_BIT;
                    end

                    RW_STOP_BIT:
                    begin
                        /* place crc into reset */
                        crc_reset <= 1'b0;

                        /* send stop bit */
                        data_out <= 4'b1111;

                        /* move to status receive state */
                        state <= STATE_STATUS_RECV;
                        rw_state <= SR_START;
                    end

                    endcase
                end
                else
                begin
                    /* disable the ram_enable if it is set */
                    if(ram_enable)
                        ram_enable <= 1'b0;

                    /* disable crc_enable if set */
                    if(crc_enable)
                        crc_enable <= 1'b0;
                end
            end

            STATE_STATUS_RECV:
            begin
                case(rw_state)
                SR_START:
                begin
                    /* read on rising edge */
                    if(clock_strobe && !sclk)
                    begin
                        /* disable data output */
                        data_enable <= 1'b0;

                        /* reset counter */
                        bit_counter <= 4'h0;

                        /* move to next state */
                        rw_state <= SR_START_BIT;
                    end
                end

                SR_START_BIT:
                begin
                    /* read on rising edge */
                    if(clock_strobe && !sclk)
                    begin
                        /* start bit is low */
                        if(data_in[0]==1'b0)
                        begin
                            /* reset counter */
                            bit_counter <= 4'h0;

                            /* move to next state */
                            rw_state <= SR_STATUS;
                        end
                        else
                        begin
                            /* check for timeout */
                            if(bit_counter==4'd8) /* TODO: parameterise */
                            begin
                                /* set status to high */
                                status <= 3'b111;

                                /* move to process */
                                rw_state <= SR_PROCESS;
                            end

                            /* increment counter */
                            bit_counter <= bit_counter + 1'b1;
                        end
                    end
                end

                SR_STATUS:
                begin
                    /* read on rising edge */
                    if(clock_strobe && !sclk)
                    begin
                        /* read in bit */
                        status <= {status[1:0], data_in[0]};

                        /* increment counter */
                        bit_counter <= bit_counter + 1'b1;

                        /* check for end condition */
                        if(bit_counter==4'd2)
                            /* move to next state */
                            rw_state <= SR_END_BIT;
                    end
                end

                SR_END_BIT:
                begin
                    /* read on rising edge */
                    if(clock_strobe && !sclk)
                    begin
                        /* check that the end bit is correctly received */
                        if(data_in[0]!=1'b1)
                            /* raise error */
                            error <= error | ERROR_NO_END_BIT;

                        /* move to next state */
                        rw_state <= SR_PROCESS;
                    end
                end

                SR_PROCESS:
                begin
                    case(status)
                    /* positive crc - data received correctly */
                    3'b010: ; //error <= ERROR_NONE;
                    /* negative crc - bad crc */
                    3'b101: error <= error | ERROR_CRC;
                    /* anything else - including all high, which is a flash programming error */
                    default: error <= error | ERROR_OTHER;
                    endcase

                    /* move to idle state */
                    state <= STATE_IDLE;
                end

                endcase
            end

            default: state <= STATE_IDLE;
            endcase
        end
    end

endmodule
