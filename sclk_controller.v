`timescale 1ns / 1ps

module sclk_controller(
     input     clk,
     input     reset,
     input     clock_strobe,
     input     enable,
     input     last_clocks_prevent,
    output reg sclk,
    output     last_clocks
    );

    initial sclk=1'b1;

    reg [2:0] clock_counter=3'd0;

    reg count_enable=1'b0;
    reg last_enable=1'b0;

    wire falling_enable;
    assign falling_enable=(!enable && last_enable && !last_clocks_prevent)?1'b1:1'b0;

    assign last_clocks=(falling_enable || count_enable)?1'b1:1'b0;

    always @(posedge clk or negedge reset)
    begin
        if(!reset)
        begin
            sclk <= 1'b1;
            last_enable <= 1'b0;
            count_enable <= 1'b0;
            clock_counter <= 3'd0;
        end
        else if(clock_strobe)
        begin
            /* update last enable */
            last_enable <= enable;

            /* reset counter on falling edge of enable */
            if(falling_enable)
            begin
                clock_counter <= 3'd0;
                count_enable <= 1'b1;
            end

            if(enable)
                count_enable <= 1'b0;

            /* update counter, if necessary */
            if(count_enable && !sclk)
            begin
                clock_counter <= clock_counter + 1'b1;
                if(clock_counter==3'd7)
                    count_enable <= 1'b0;
            end

            /* sclk output */
            if(enable || falling_enable || count_enable)
                sclk <= ~sclk;
        end
    end

endmodule
