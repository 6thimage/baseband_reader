`timescale 1ns / 1ps

module crc16_2(
     input            clk,
     input            reset,
     input            enable,
     input     [1:0]  data,
    output reg [15:0] crc
    );

    initial crc=16'h0;

    always @(posedge clk or negedge reset)
    begin
        if(!reset)
            /* on reset, set the crc to zero */
            crc <= 16'h0;
        else if(enable)
        begin
            /* process input data byte, if enabled */
            crc[0]  <= crc[14] ^ data[0];
            crc[1]  <= crc[15] ^ data[1];
            crc[2]  <= crc[0];
            crc[3]  <= crc[1];
            crc[4]  <= crc[2];
            crc[5]  <= crc[3]  ^ crc[14] ^ data[0];
            crc[6]  <= crc[4]  ^ crc[15] ^ data[1];
            crc[7]  <= crc[5];
            crc[8]  <= crc[6];
            crc[9]  <= crc[7];
            crc[10] <= crc[8];
            crc[11] <= crc[9];
            crc[12] <= crc[10] ^ crc[14] ^ data[0];
            crc[13] <= crc[11] ^ crc[15] ^ data[1];
            crc[14] <= crc[12];
            crc[15] <= crc[13];
        end
    end

endmodule
