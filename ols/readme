The data captured by OLS titled '4 block playback' is blocks 1-4 of the
SD card. The baseband reader has read in, and discarded, block 0. The
'5block.dat' file is the first 5 blocks of data - therefore, the data
captured by OLS starts at 0x200.

From the playback speed capture, the playing back of 512 bytes (1024
samples) takes 61.44 us - this is consistent throughout the capture.
The time taken between buffer swaps is typically 20 ns (1 clock cycle),
this is shown by the idle signal pulsing once. There is an exception to
this for the 16th read block where the idle pulse is 118.68 us long.
From several runs of the test (both cold and warm), this longer pulse
occurs in the same place - and therefore is to do with the architecture
of the used SD card. This higher length pulse implies that the 16th
read is taking around 180 us and is quite likely due to the SD card
changing pages.

In larger tests, similar blips like that of the 16th block occur around
once per second (with a data rate of almost 8 MB/s). These are
typically larger blips at around 180 us (implying a read is taking
roughly 250 us).
