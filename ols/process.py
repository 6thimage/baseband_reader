#! /usr/bin/env python

with open('4 block playback.ols', 'r') as f:
    toggle=0
    data=0
    count=0
    for line in f.readlines():
        if line[0]==';':
            continue
        loc=line.find('@')
        if loc>0:
            val=int(line[:loc], 16)
            if not val&1:
                continue
            val=(val>>1)&0xf
            # ols order Q0 Q1 I0 I1
            # baseband format is I1 I0 Q1 Q0
            val=(val&1)<<3 | (val&2)<<1 | (val&4)>>1 | (val&8)>>3
            if toggle:
                data=(data&0xf0)|val
                toggle=0
                print '{:02x}'.format(data),
                count+=1
                if count>9:
                    print
                    count=0
            else:
                data=(data&0xf)|(val<<4)
                toggle=1
