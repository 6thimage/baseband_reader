#! /bin/bash

# create ols output
./process.py > ols.out

# create hex dump
xxd -g 1 -c 10 -seek 0x200 5block.dat|cut -c10-38 > baseband.out

# compare
meld ols.out baseband.out &>/dev/null

# remove
rm ols.out baseband.out
