`timescale 1ns / 1ps

module clk_strobe #(
    parameter div_len=8
    )
    (
     input                   clk,
     input                   reset,
     input     [div_len-1:0] clk_div,
    output reg               clk_strobe
    );

    initial clk_strobe=1'b0;

    reg [div_len-1:0] clock_counter='h0;

    always @(posedge clk or negedge reset)
    begin
        if(!reset)
        begin
            clock_counter <= 'h0;
            clk_strobe <= 1'b0;
        end
        else
        begin
            if(clock_counter >= clk_div)
            begin
                clock_counter <= 'h0;
                clk_strobe <= 1'b1;
            end
            else
            begin
                clock_counter <= clock_counter + 1'b1;
                clk_strobe <= 1'b0;
            end
        end
    end

endmodule
