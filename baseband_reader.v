module baseband_reader(
     input clk,
     input reset,
    /* sd card */
    output sd_clk,
     inout sd_cmd,
     inout [3:0] sd_data,
    /* baseband */
    output reg data_valid,
    output reg [1:0] I, Q,
    /* control */
     input [31:0] start_block, /* block to start read with */
     input [31:0] number_of_blocks, /* number of blocks to read, or zero for all */
     input enable_playback,
     input allow_restart,
    /* outputs */
    output idle,
    output error
    );

    /* double buffered ram */
    reg ram_buffer=1'b0;
    wire sd_ram_clk, sd_ram_enable, sd_ram_write_enable;
    wire [8:0] sd_ram_address;
    wire [7:0] sd_ram_in, sd_ram_out;
    reg ram_enable=1'b0;
    reg [8:0] ram_address={9{1'b1}};
    wire [7:0] ram_data_out;
    /* as we are only reading, the B port has the write enable and data in tied low */
    ram_double_buffer #(.data_width(8), .address_width(9)) ram
                       (.buffer(ram_buffer),
                        .A_clk(sd_ram_clk), .A_enable(sd_ram_enable),
                            .A_write_enable(sd_ram_write_enable), .A_address(sd_ram_address),
                            .A_data_in(sd_ram_in), .A_data_out(sd_ram_out),
                        .B_clk(clk), .B_enable(ram_enable), .B_write_enable(1'b0),
                            .B_address(ram_address), .B_data_in(8'd0), .B_data_out(ram_data_out)
                       );

    /* sd card controller */
    reg sd_data_enable=1'b0, sd_last=1'b0, sd_data_next=1'b0;
    wire multiple_blocks=(number_of_blocks==1)?1'b0:1'b1;
    wire fixed_count=(number_of_blocks==0)?1'b0:1'b1;
    wire [3:0] sd_error;
    wire sd_idle, sd_data_idle;
    wire [31:0] sd_size;
    sd_controller sd_card(.clk(clk), .reset(reset),
                          /* sd card */
                          .sclk(sd_clk), .cmd(sd_cmd), .data(sd_data), .card_detect(1'b1),
                          /* ram */
                          .ram_clk(sd_ram_clk), .ram_enable(sd_ram_enable),
                              .ram_write_enable(sd_ram_write_enable), .ram_address(sd_ram_address),
                              .ram_data_in(sd_ram_in), .ram_data_out(sd_ram_out),
                          /* control */
                          .data_command_enable(sd_data_enable), .write_block(1'b0),
                              .erase_blocks(1'b0), .block_address(start_block),
                              .multiple_blocks(multiple_blocks), .fixed_count(fixed_count),
                              .block_count(number_of_blocks), .last_block(sd_last),
                              .data_next(sd_data_next),
                          /* outputs */
                          .error(sd_error), .idle(sd_idle), .data_next_idle(sd_data_idle),
                              .card_version_2(), .card_hc(), .card_uhs(), .card_block_size(sd_size)
                         );

    /* read command control */
    localparam STATE_INIT=0, STATE_READ_WAIT=1, STATE_IDLE=2, STATE_ERROR=3;
    reg [1:0] state=STATE_INIT;
    wire read_idle=(state==STATE_IDLE)?1'b1:1'b0;
    assign error=(state==STATE_ERROR)?1'b1:1'b0;
    always @(posedge clk or negedge reset)
    begin
        if(!reset)
        begin
            /* reset sd card data enable */
            sd_data_enable <= 1'b0;

            /* reset state */
            state <= STATE_INIT;
        end
        else
        begin
            /* process
             * - wait for card initialisation to complete
             * - send read command
             * - wait for end of read & restart flag
             */
            case(state)
            STATE_INIT:
            begin
                if(sd_idle)
                begin
                    if(sd_error!='d0)
                        state <= STATE_ERROR;
                    else
                    begin
                        /* send read command */
                        sd_data_enable <= 1'b1;
                        state <= STATE_READ_WAIT;
                    end
                end
            end

            STATE_READ_WAIT:
            begin
                if(sd_data_enable && !sd_idle)
                    sd_data_enable <= 1'b0;
                else if(!sd_data_enable)
                begin
                    if(sd_data_idle)
                        state <= STATE_IDLE;
                    else if(sd_idle)
                    begin
                        if(!multiple_blocks)
                            state <= STATE_IDLE;
                        else
                            state <= STATE_ERROR;
                    end
                end
            end

            STATE_IDLE:
            begin
                if(sd_idle && allow_restart)
                begin
                    /* send read command */
                    sd_data_enable <= 1'b1;
                    state <= STATE_READ_WAIT;
                end
            end

            STATE_ERROR:
            begin
            end
            endcase
        end
    end

    /* states */
    localparam BUFFER_IDLE=0, BUFFER_FETCH=1;
    reg buffer_state=BUFFER_IDLE;
    wire buffer_idle=(buffer_state==BUFFER_IDLE)?1'b1:1'b0;
    localparam PLAYBACK_IDLE=0, PLAYBACK_NOP=1, PLAYBACK_FETCH=2, PLAYBACK_SEND=3;
    reg [1:0] playback_state=PLAYBACK_IDLE;
    wire playback_idle=(playback_state==PLAYBACK_IDLE)?1'b1:1'b0;
    //assign idle=&{read_idle, buffer_idle, playback_idle};
    assign idle=playback_idle;
    /* block counter */
    reg [31:0] block_counter='d0;
    reg block_end_condition, block_last_condition;
    always @(*)
    begin
        block_last_condition=1'b0;
        block_end_condition=1'b0;

        if(number_of_blocks==0)
        begin
            if(block_counter >= (sd_size-start_block-1))
                block_last_condition=1'b1;
            if(block_counter >= (sd_size-start_block))
                block_end_condition=1'b1;
        end
        else
        begin
            if(block_counter >= (number_of_blocks-1))
                block_last_condition=1'b1;
            if(block_counter >= number_of_blocks)
                block_end_condition=1'b1;
        end
    end

    /* sd_idle tracking */
    reg sd_idle_last=1'b0;
    always @(posedge clk) sd_idle_last <= sd_idle;
    wire sd_idle_pulse=(!sd_idle_last && sd_idle)?1'b1:1'b0;
    /* buffer status */
    reg buffer_empty=1'b0;
    reg play_nibble=1'b0;
    reg single_shot_done=1'b0;
    always @(posedge clk or negedge reset)
    begin
        if(!reset)
        begin
            /* reset ram buffer */
            ram_buffer <= 1'b0;

            /* reset sd data control */
            sd_last <= 1'b0;
            sd_data_next <= 1'b0;

            /* states */
            buffer_state <= BUFFER_IDLE;
            playback_state <= PLAYBACK_IDLE;

            block_counter <= 'd0;
            single_shot_done <= 1'b0;

            /* ram */
            ram_enable <= 1'b0;
            ram_address <= {9{1'b1}};

            /* buffer state */
            buffer_empty <= 1'b0;
            play_nibble <= 1'b0;
        end
        else
        begin
            /* buffer control */
            case(buffer_state)
            BUFFER_IDLE:
            begin
                if(sd_data_idle && buffer_empty && !block_end_condition)
                begin
                    sd_data_next <= 1'b1;

                    if(block_last_condition)
                        sd_last <= 1'b1;
                    else
                        sd_last <= 1'b0;

                    buffer_state <= BUFFER_FETCH;
                end
                else if(idle && !multiple_blocks && !single_shot_done)
                begin
                    buffer_empty <= 1'b0;
                    single_shot_done <= 1'b1;
                end
            end

            BUFFER_FETCH:
            begin
                if(sd_data_next && !sd_data_idle)
                    sd_data_next <= 1'b0;
                else if(!sd_data_next && sd_data_idle)
                begin
                    buffer_empty <= 1'b0;

                    block_counter <= block_counter +1'b1;

                    buffer_state <= BUFFER_IDLE;
                end
            end
            endcase

            /* reset data valid */
            data_valid <= 1'b0;

            /* disable reading */
            ram_enable <= 1'b0;

            /* playback */
            case(playback_state)
            PLAYBACK_IDLE:
            begin
                if(!buffer_empty && read_idle)
                begin
                    /* swap buffer */
                    ram_buffer <= ~ram_buffer;
                    /* mark as empty */
                    buffer_empty <= 1'b1;

                    /* reset ram address */
                    ram_address <= 9'd0;
                    play_nibble <= 1'b0;

                    /* move to next state */
                    playback_state <= PLAYBACK_FETCH;
                end
            end

            PLAYBACK_FETCH:
            begin
                /* read from ram */
                if(!play_nibble)
                    ram_enable <= 1'b1;

                /* move to next state */
                playback_state <= PLAYBACK_NOP;
            end

            PLAYBACK_NOP: playback_state <= PLAYBACK_SEND;

            PLAYBACK_SEND:
            begin
                if(enable_playback)
                begin
                    /* send data */
                    if(!play_nibble)
                        {I, Q} <= ram_data_out[7:4];
                    else
                        {I, Q} <= ram_data_out[3:0];
                    data_valid <= 1'b1;

                    /* switch to next nibble */
                    play_nibble <= ~play_nibble;

                    /* increment ram pointer */
                    if(play_nibble)
                        ram_address <= ram_address + 1'b1;

                    /* check for end condition */
                    if(play_nibble && ram_address == {9{1'b1}})
                        playback_state <= PLAYBACK_IDLE;
                    else
                        playback_state <= PLAYBACK_FETCH;
                end
            end
            endcase

            /* reset and start condition *
            if(sd_idle_pulse)
            begin
                buffer_state <= BUFFER_IDLE;
                playback_state <= PLAYBACK_IDLE;
                buffer_empty <= 1'b1;
                single_shot_done <= 1'b0;
            end/**/
        end
    end

endmodule
